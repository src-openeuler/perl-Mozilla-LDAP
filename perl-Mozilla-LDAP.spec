Name:           perl-Mozilla-LDAP
Version:        1.5.3
Release:        29
Summary:        Wraps OpenLDAP C SDK as LDAP Perl module
License:        GPLv2+ and LGPLv2+ and MPLv1.1
URL:            https://www-archive.mozilla.org/directory/faq/perldap-faq
Source0:        https://ftp.mozilla.org/pub/mozilla.org/directory/perldap/releases/%{version}/src/perl-mozldap-%{version}.tar.gz
Source1:        https://ftp.mozilla.org/pub/mozilla.org/directory/perldap/releases/1.5/src/Makefile.PL.rpm
Requires:       perl-interpreter >= 2:5.8.0
BuildRequires:  gcc perl-interpreter >= 2:5.8.0 perl-devel perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) nspr-devel nss-devel openldap-devel >= 2.4.22

%description
Wraps OpenLDAP C SDK as LDAP Perl module.

%prep
%autosetup -n perl-mozldap-%{version} -p1
cat << \EOF > perl-Mozilla-LDAP-prov
#!/bin/sh
%{__perl_provides} $* |\
  sed -e '/perl(Mozilla::LDAP::Entry)$/d'
EOF

%define __perl_provides %{_builddir}/perl-mozldap-%{version}/perl-Mozilla-LDAP-prov
chmod +x %{__perl_provides}

cat << \EOF > perl-Mozilla-LDAP-req
%{__perl_requires} $* |\
  sed -e '/perl(Mozilla::LDAP::Entry)/d'
EOF

%define __perl_requires %{_builddir}/perl-mozldap-%{version}/perl-Mozilla-LDAP-req
chmod +x %{__perl_requires}

%build
LDAPPKGNAME=openldap CFLAGS="$RPM_OPT_FLAGS" perl %{SOURCE1} PREFIX=$RPM_BUILD_ROOT%{_prefix} INSTALLDIRS=vendor < /dev/null
make OPTIMIZE="$RPM_OPT_FLAGS" CFLAGS="$RPM_OPT_FLAGS"
make test

%install
eval `perl '-V:installarchlib'`
%makeinstall
rm -f `find $RPM_BUILD_ROOT -type f -name perllocal.pod -o -name .packlist`
find $RPM_BUILD_ROOT -name API.so -exec chmod 755 {} \;

if [ -x /usr/lib/rpm/brp-compress ] ; then
    /usr/lib/rpm/brp-compress
fi

find $RPM_BUILD_ROOT%{_prefix} -type f -print | \
    sed "s@^$RPM_BUILD_ROOT@@g" > perl-Mozilla-LDAP-%{version}-%{release}-filelist
if [ "$(cat perl-Mozilla-LDAP-%{version}-%{release}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit 1
fi

%files -f perl-Mozilla-LDAP-%{version}-%{release}-filelist
%doc CREDITS ChangeLog README MPL-1.1.txt
%exclude %{_datadir}/perl5/
%exclude %{_libdir}/perl5/vendor_perl/auto/Mozilla/LDAP/API/API.bs

%changelog
* Fri Jun 18 2021 zhaoyao<zhaoyao32@huawei.com> - 1.5.3-29
- Remove redundant dependencies, Git is only for patching, user patches instead.

* Sat Apr 18 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.5.3-28
- Package init
